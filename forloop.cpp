#include <iostream>
#include "forloop.h"

void call_loop(int count) {
	std::cout << "Count = " << count << std::endl;
	for (int i = count; i > 0; i--) {
		std::cout << "Count: " << i << std::endl;
	}
}
